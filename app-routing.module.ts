import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BooksComponent } from './admin/books/books.component';
import { UsersComponent } from './admin/users/users.component';
import { HomeComponent } from './home/home.component';

import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { RegisterComponent } from './register/register.component';
import { UserhomeComponent } from './userhome/userhome.component';

const routes: Routes = [
  {path:"",component:MenuComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path: 'home/admin/users', component: UsersComponent},
  {path: 'home/admin/books', component: BooksComponent},
  {path:'home',component:HomeComponent},
  {path:'userhome',component:UserhomeComponent},
  {path:'cart',component:CartComponent},
  {path:'addbook',component:AddbookComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
