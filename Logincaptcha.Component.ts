import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logincaptcha',
  templateUrl: './logincaptcha.component.html',
  styleUrls: ['./logincaptcha.component.css']
})
export class LogincaptchaComponent implements OnInit{
  captcha: string;
  msg: string;
  constructor() {
    this.captcha='';
    this.msg='verified human'
  }

  ngOnInit(): void {
    
  }
  
  resolved(captchaResponse: string){
    this.captcha=captchaResponse;
    console.log('resolved captcha with response:'+ this.captcha)
  }
}
