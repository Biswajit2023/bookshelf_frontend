
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Books } from '../model/Book';
import { User } from '../model/User';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  [x: string]: any;

  constructor(
    private httpClient:HttpClient)
     { 
    
     }
     getUsers()
     {
       return this.httpClient.get('http://localhost:8080/main/users');
     }

     getUserData(username:User,password:User)
     {
       return this.httpClient.get('http://localhost:8080/main/users/'+username+'/'+password)
     }

     addUser(newUser: User) {
      return this.httpClient.post<User[]>('http://localhost:8080/main/users', newUser);
    }

    getAllBooks()
    {
      return this.httpClient.get<Books[]>("http://localhost:8080/main/books")
    }

    addBook(newBook: Books) {
      return this.httpClient.post<Books>('http://localhost:8080/main/books', newBook);
    }
}


