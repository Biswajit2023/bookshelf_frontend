import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  public cartItems:any=[];
  public productList=new BehaviorSubject<any>([]);
  public search=new BehaviorSubject<string>("");

  constructor() { }

  getProducts()
  {
    return this.productList.asObservable();
  }
  setProducts(Product:any)
  {
    this.cartItems.push(... Product)
     this.productList.next(Product);
  }
  addToCart(Product:any)
  { 
    this.cartItems.push(Product);
    this.productList.next(this.cartItems);
    this.getTotal();
    console.log(this.cartItems);

  }
  getTotal():number
  {
       let grandtotal=0;
       this.cartItems.map((a:any)=>{
        grandtotal +=a.total;
       })
       return grandtotal;
  }

  removeCartItem(Product:any)
  {
    this.cartItems.map((a:any,index:any)=>
    {
      if(Product.id=== a.id)
      {
        this.cartItems.splice(index,1);
      }
    })
    this.productList.next(this.cartItems);

  }
}
