import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  paymentHandler:any = null;
  public product:any=[];
  public grandTotal!:number;
  constructor(private cartservice:CartService,private route:Router,
    private notifyService : NotificationService){

  }
  ngOnInit(): void {
    this.invokeStripe();
    this.cartservice.getProducts().subscribe(res=>{
      this.product=res;
      this.grandTotal=this.cartservice.getTotal();
    })
    
  }
  initializePayment(amount: number) {
    const paymentHandler = (<any>window).StripeCheckout.configure({
      key: 'pk_test_sLUqHXtqXOkwSdPosC8ZikQ800snMatYMb',
      locale: 'auto',
      token: function (stripeToken: any) {
        console.log({stripeToken})
        alert('Your order wil be deliverd within 7 days');
      }
    });

  
    paymentHandler.open({
      name: 'Bookshelf',
      description: 'Buying a book',
      amount : this.grandTotal
    });
  }
  
  invokeStripe() {
    if(!window.document.getElementById('stripe-script')) {
      const script = window.document.createElement("script");
      script.id = "stripe-script";
      script.type = "text/javascript";
      script.src = "https://checkout.stripe.com/checkout.js";
      script.onload = () => {
        this.paymentHandler = (<any>window).StripeCheckout.configure({
          key: 'pk_test_51MmsWISAyCFa8rBAV5KEZWxkJnFzfPmxlHG6gh65w5ZXtWikgNLPh0QUsnDnlHVWNBcmGYAC9Iy7SRz6bEEAEHXx00a3Dp2lSz',
          locale: 'auto',
          token: function (stripeToken: any) {
            console.log(stripeToken)
            alert('Payment has been successfull!');
          }
        });
      }
      window.document.body.appendChild(script);
    }
  }



  removeItem(p:any)
  {
    this.cartservice.removeCartItem(p);

  }
  shopmore(){
        this.route.navigateByUrl("userhome")
  }

  buy(){

  }

}
