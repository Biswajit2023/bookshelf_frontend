import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { User } from '../model/User';
import { ServiceService } from '../service.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit{
 


  @Input()
  user: User;

  @Output()
  userAddedEvent = new EventEmitter();
  ngOnInit(): void {

  }

  constructor(private httpClientService: ServiceService, private router: Router,
    private toastr: ToastrService) {
    this.user = new User ;
   }

   addUser() {
    this.httpClientService.addUser(this.user).subscribe(
      (user) => {
        this.toastr.success("Registration Successful !!")
        this.router.navigateByUrl("login"); 
      }
    ); error:(err: any)=>this.toastr.error('Registration failed')
   
  }



}
