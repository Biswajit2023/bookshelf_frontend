
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';
import { HttpClientService } from '../service/http-client.service';

@Component({
  selector: 'app-userhome',
  templateUrl: './userhome.component.html',
  styleUrls: ['./userhome.component.css']
})
export class UserhomeComponent implements OnInit {
  serachKey:string="";
  public searchTerm:string="";
  public productList:any;
 public totalItem:number=0;
  constructor(private api:HttpClientService,private cartservice:CartService,
    private route:Router)
  {

  }
  ngOnInit(): void {
   this.cartservice.getProducts().subscribe(res=>
    {
      this.totalItem=res.length;
    });

  this.api.getAllBooks().subscribe(res=>{
    this.productList=res;

    this.productList.forEach((a:any) => {
      Object.assign(a,{quantity:1,total:a.bookprice});
    });
  })

  this.cartservice.search.subscribe((val:any)=>{
    this.serachKey=val;
  })
  }

  search(event:any)
  {
    this.searchTerm=(event.target as HTMLInputElement).value;
    this.cartservice.search.next(this.searchTerm);

  }
  addToCart(p:any)
  {
       this.cartservice.addToCart(p);
  }
  logout()
  {
    alert("logout succesfully");
  }

  showcart()
  {
     this.route.navigateByUrl("cart");
  }

}
